from nginx:alpine

run apk add curl

copy index.html /usr/share/nginx/html/

copy index.html /var/www/html/

copy default.conf /etc/nginx/conf.d/

copy default.conf /etc/nginx/sites-available/

copy default.conf /etc/nginx/sites-enabled/

expose 80:80

CMD ["nginx", "-g", "daemon off;"]
